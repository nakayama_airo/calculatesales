package jp.alhinc.nakayama_airo.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays; 
import java.util.HashMap;
import java.util.TreeMap;

public class CalculateSales {
	public static void main(String[] args) {

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		TreeMap<String, String> branchMap = new TreeMap<String, String>();
		HashMap<String, Long> totalMap = new HashMap<String, Long>();

		//  以降、branch.lstの作成。

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			String line;
			while ((line = br.readLine()) != null) {            	// line <- 001,札幌支店
				if (!line.isEmpty()) {
					String[] codeBranch = line.split(",");         //001,札幌支店
					if ((codeBranch.length != 2) || (!codeBranch[0].matches("[0-9]{3}"))) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
					branchMap.put(codeBranch[0], codeBranch[1]);
					totalMap.put(codeBranch[0], 0L);
				}
			}

		} catch (IOException e) {
		    System.out.println("予期せぬエラーが発生しました");
		    return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
				    System.out.println("予期せぬエラーが発生しました");
				    return;
			    }
			}
		}

		// 以降、.rcdの作成。

		File dir = new File(args[0]);
		
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String name) {
				return file.isFile() && name.matches("^[0-9]{8}.rcd$");
			}
		};
		File[] files = dir.listFiles(filter);
		
		Arrays.sort(files);
		for (int i = 0; i < files.length - 1; i++) {
			String fileName1 = files[i].getName().substring(0, 8);

			int fileNameInt1 = Integer.parseInt(fileName1);
			if (fileNameInt1 != i + 1) {
				System.out.println("売上ファイル名が連番になっていません"); //ファイル名を引いた数が1でない
				return;
			}
		}

		BufferedReader rcdBr = null;
		for(int i = 0; i < files.length; i++) {
			try {
				String fileName = files[i].getName();

				File file = new File(args[0], fileName);
				FileReader fr = new FileReader(file);
				rcdBr = new BufferedReader(fr);

				String line;

				ArrayList<String> codeSales = new ArrayList<String>();
				while((line = rcdBr.readLine()) != null) { // line <- 001,札幌支店
					codeSales.add(line);
				}
				if (codeSales.size() != 2) {
					System.out.println(fileName + "のフォーマットが不正です"); //2行以外
					return;
				}
				if (!codeSales.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました"); //数字以外
					return;
				}
				if (!totalMap.containsKey(codeSales.get(0))) {
					System.out.println(fileName + "の支店コードが不正です"); //支店コードが支店名義ファイルに存在しない
					return;
				}
				if (!codeSales.get(0).isEmpty()) {
					long sales =  Long.parseLong(codeSales.get(1));
					long newTotal = totalMap.get(codeSales.get(0)) + sales;
					if (newTotal >= 10000000000L) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}

					totalMap.put(codeSales.get(0), newTotal);

				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (rcdBr != null) {
					try {
						 rcdBr.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!writerBranchOut("branch.out",args[0],branchMap,totalMap)) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		// 以降、branch.outの作成。

	}
	public static boolean writerBranchOut(String fileName, String folder, TreeMap<String, String> branchMap, HashMap<String, Long> totalMap) {
		
		BufferedWriter writer = null;
		try {
			File file = new File(folder, fileName);
			FileWriter fw = new FileWriter(file);
			writer = new BufferedWriter(fw);

			for(String code : branchMap.keySet()) {
				String Line = String.format("%s,%s,%d", code, branchMap.get(code), totalMap.get(code));
				writer.write(Line);
				writer.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if (writer != null) {
					writer.close();
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
}